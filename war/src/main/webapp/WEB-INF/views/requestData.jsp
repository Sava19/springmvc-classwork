<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html xmlns:form="http://www.w3.org/1999/html">
    <head>
        <title>HelloWorld</title>
    </head>

    <body>
    <p>введите даты для поиска</p>
        <form:form method="POST" action="/maven-servlet/mvc/result" modelAttribute="inputData">
            <table>
                <tr/>
                <tr>
                <form:radiobutton path="range" value="true" /> Выбрать диапазон
                <table bgcolor="#faebd7">
                    <tr>
                        <td><form:label path="startDate">Start range</form:label></td>
                        <td><form:input path="startDate"/></td>
                        <td><form:label path="endDate">End range</form:label></td>
                        <td><form:input path="endDate"/></td>
                    </tr>
                    <tr>
                        <td><input type="submit" value="Go to result"/></td>
                    </tr>
                </table>
                </tr>
                <tr>
                    <form:radiobutton path="range" value="false" /> Выбрать дату
                    <table bgcolor="#5f9ea0">
                        <tr>
                            <td><form:label path="singleDate">enter date</form:label></td>
                            <td><form:input path="singleDate"/></td>
                        </tr>
                        <tr>
                            <td><input type="submit" value="Go to result"/></td>
                        </tr>
                    </table>
                </tr>
            </table>
        </form:form>
    </body>

</html>